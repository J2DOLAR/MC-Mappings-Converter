package me.j2dolar.mcmc.mappings;

import me.j2dolar.mcmc.containers.ClassMapping;
import me.j2dolar.mcmc.containers.FieldMapping;
import me.j2dolar.mcmc.containers.MethodMapping;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SrgMappingsParser implements MappingsParser {

    @Override
    public List<ClassMapping> read(File origin) {
        if (!origin.exists() || !origin.isDirectory())
            return null;

        File joinedSrgFile = new File(origin + "joined.srg");
        File fieldsCsvFile = new File(origin + "fields.csv"); // used to actually deobf the field names later on
        File methodsCsvFile = new File(origin + "methods.csv"); // ||                             method ||
        File paramsCsvFile = new File(origin + "params.csv"); // ||                               params ||

        List<ClassMapping> classMappingList = new ArrayList<>();
        Map<String, ClassMapping> classMappingsByObf = new HashMap<>();
        Map<String, FieldMapping> fieldMappingsByDeobfName = new HashMap<>();
        Map<String, MethodMapping> methodMappingsByDeobfName = new HashMap<>();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(joinedSrgFile));
            String next;
            while ((next = reader.readLine()) != null) {
                String[] parts = next.split(" ");
                switch (parts[0]) {
                    case "CL:":
                        ClassMapping classMapping = new ClassMapping(parts[1], parts[2]);
                        classMappingList.add(classMapping);
                        classMappingsByObf.put(parts[1], classMapping);
                        break;
                    case "FD:": {
                        int lastSlashObf = parts[1].lastIndexOf('/');
                        int lastSlashDeobf = parts[2].lastIndexOf('/');
                        String classObf = parts[1].substring(0, lastSlashObf);
                        String fieldObf = parts[1].substring(lastSlashObf + 1);
                        String fieldDeobf = parts[2].substring(lastSlashDeobf + 1);
                        FieldMapping fieldMapping = new FieldMapping(fieldObf, fieldDeobf, null, null);
                        classMappingsByObf.get(classObf).registerFieldMapping(fieldMapping);
                        fieldMappingsByDeobfName.put(fieldDeobf, fieldMapping);
                        break;
                    }
                    case "MD:": {
                        int lastSlashObf = parts[1].lastIndexOf('/');
                        int lastSlashDeobf = parts[3].lastIndexOf('/');
                        String classObf = parts[1].substring(0, lastSlashObf);
                        String methodObf = parts[1].substring(lastSlashObf + 1);
                        String methodDeobf = parts[3].substring(lastSlashDeobf + 1);
                        MethodMapping methodMapping = new MethodMapping(methodObf, methodDeobf, parts[2], parts[4]);
                        classMappingsByObf.get(classObf).registerMethodMapping(methodMapping);
                        methodMappingsByDeobfName.put(methodDeobf, methodMapping);
                        break;
                    }
                }
            }

            reader.close();
            reader = new BufferedReader(new FileReader(fieldsCsvFile));
            reader.readLine(); // ignore the first line
            while ((next = reader.readLine()) != null) {
                String[] parts = next.split(","); // we dont care about the description
                // searge,name,side,desc
                if (fieldMappingsByDeobfName.containsKey(parts[0])) {
                    fieldMappingsByDeobfName.get(parts[0]).setDeobfuscatedName(parts[1]);
                }
            }
            reader.close();
            reader = new BufferedReader(new FileReader(paramsCsvFile));
            reader.readLine(); // ignore the first line
            while ((next = reader.readLine()) != null) {
                // param,name,side
                // todo: use the numbers in p_<#>_<#>_ to fix the arguments
            }
            reader.close();
            reader = new BufferedReader(new FileReader(methodsCsvFile));
            reader.readLine(); // ignore the first line
            while ((next = reader.readLine()) != null) {
                String[] parts = next.split(",");
                // searge,name,side,desc
                if (methodMappingsByDeobfName.containsKey(parts[0])) {
                    methodMappingsByDeobfName.get(parts[0]).setDeobfuscatedName(parts[1]);
                }
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return classMappingList;
    }

    @Override
    public void write(File destination, List<ClassMapping> mappedClasses) {
        // todo
    }
}
