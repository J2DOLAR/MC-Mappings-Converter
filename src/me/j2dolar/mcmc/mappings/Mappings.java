package me.j2dolar.mcmc.mappings;

import me.j2dolar.mcmc.containers.ClassMapping;
import me.j2dolar.mcmc.containers.FieldMapping;
import me.j2dolar.mcmc.containers.MethodMapping;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * Although MC Mappings Converter is intended to be a command line utility,
 * I thought a little class like this would be useful for anyone trying to
 * use this project as a library for whatever it is they're making.
 */
public class Mappings {

    private final Map<String, ClassMapping> classesByDeobf = new HashMap<>();
    private final Map<String, Map<String, MethodMapping>> methodsByDeobfClassDeobfMethod = new HashMap<>();
    private final Map<String, Map<String, FieldMapping>> fieldsByDeobfClassDeobfField = new HashMap<>();

    public Mappings(List<ClassMapping> classMappings) {
        populate(classMappings);
    }

    public ClassMapping getClassMapping(String deobfName) {
        return this.classesByDeobf.get(deobfName);
    }

    public MethodMapping getMethodMapping(String deobfClassName, String deobfMethodName) {
        return this.methodsByDeobfClassDeobfMethod.get(deobfClassName).get(deobfMethodName);
    }

    public FieldMapping getFieldMapping(String deobfClassName, String deobfFieldName) {
        return this.fieldsByDeobfClassDeobfField.get(deobfClassName).get(deobfFieldName);
    }

    private void populate(List<ClassMapping> classMappings) {
        classMappings.forEach(classMapping -> {
            classesByDeobf.put(classMapping.getDeobfuscatedName(), classMapping);


            classMapping.getMethodMappings().forEach(methodMapping -> {
                if (!methodsByDeobfClassDeobfMethod.containsKey(classMapping.getDeobfuscatedName()))
                    methodsByDeobfClassDeobfMethod.put(classMapping.getDeobfuscatedName(), new HashMap<>());

                methodsByDeobfClassDeobfMethod.get(classMapping.getDeobfuscatedName()).put(methodMapping.getDeobfuscatedName(), methodMapping);
            });

            classMapping.getFieldMappings().forEach(fieldMapping -> {
                if (!fieldsByDeobfClassDeobfField.containsKey(classMapping.getDeobfuscatedName()))
                    fieldsByDeobfClassDeobfField.put(classMapping.getDeobfuscatedName(), new HashMap<>());

                fieldsByDeobfClassDeobfField.get(classMapping.getDeobfuscatedName()).put(fieldMapping.getDeobfuscatedName(), fieldMapping);
            });
        });
    }

}
