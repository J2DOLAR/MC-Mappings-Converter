package me.j2dolar.mcmc.mappings;

import me.j2dolar.mcmc.containers.ClassMapping;

import java.io.File;
import java.util.List;

public interface MappingsParser {

    String NEXT_LINE = System.lineSeparator();

    List<ClassMapping> read(File origin);

    void write(File destination, List<ClassMapping> mappedClasses);

}
