package me.j2dolar.mcmc.mappings;

import me.j2dolar.mcmc.containers.ClassMapping;
import me.j2dolar.mcmc.containers.FieldMapping;
import me.j2dolar.mcmc.containers.MethodMapping;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EnigmaMappingsParser implements MappingsParser {

    private static Pattern NONE_PATTERN = Pattern.compile("^none/([^0-9]+[a-zA-Z0-9/]*)");

    @Override
    public List<ClassMapping> read(File origin) {
        List<ClassMapping> classMappings = new ArrayList<>(128);

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(origin));

            String next;
            ClassMapping lastClass = null;
            MethodMapping lastMethod = null;

            while ((next = bufferedReader.readLine()) != null) {
                next = next.trim();
                String[] parts = next.split(" ");

                switch (parts[0]) {
                    case "CLASS":
                        Matcher noneMatcher = NONE_PATTERN.matcher(parts[1]);
                        if (noneMatcher.matches()) {
                            parts[1] = noneMatcher.group(1); // remove "none/" from beginning of deobfuscated name
                        }
                        lastClass = new ClassMapping(parts[1], parts.length == 2 ? null : parts[2]);
                        classMappings.add(lastClass);
                        break;
                    case "FIELD":
                        if (lastClass == null)
                            continue;

                        lastClass.registerFieldMapping(new FieldMapping(parts[1], parts[2], null, parts[3]));
                        break;
                    case "METHOD":
                        if (lastClass == null)
                            continue;

                        if (parts.length == 4) {
                            lastMethod = new MethodMapping(parts[1], parts[2], null, parts[3]);
                        } else if (parts.length == 3) {
                            lastMethod = new MethodMapping(parts[1], null, null, parts[2]);
                        }
                        lastClass.registerMethodMapping(lastMethod);
                        break;
                    case "ARG":
                        if (lastMethod == null)
                            continue;

                        lastMethod.nameParameter(Integer.parseInt(parts[1]), parts[2]);
                        break;
                }
            }

            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return classMappings;
    }

    @Override
    public void write(File destination, List<ClassMapping> mappedClasses) {
        BufferedWriter bufferedWriter = null;
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(destination));
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (bufferedWriter == null)
            return;

        final BufferedWriter out = bufferedWriter;
        mappedClasses.forEach((classMapping -> {
            try {
                out.write("CLASS ");
                if (classMapping.getObfuscatedName().indexOf('/') < 0) { // because enigma wants 'none/'
                    out.write("none/");
                }
                out.write(classMapping.getObfuscatedName());

                if (classMapping.isNameDeobfuscated()) {
                    out.write(" ");
                    out.write(classMapping.getDeobfuscatedName());
                }
                out.write(NEXT_LINE);
            } catch (IOException e) {
                e.printStackTrace();
            }

            classMapping.getFieldMappings().forEach((fm) -> {
                try {
                    out.write("\tFIELD ");
                    out.write(fm.getObfuscatedName());
                    if (fm.isNameDeobfuscated()) {
                        out.write(" ");
                        out.write(fm.getDeobfuscatedName());
                    }
                    out.write(" ");
                    out.write(fm.getDeobfuscatedType());
                    out.write(NEXT_LINE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

            classMapping.getMethodMappings().forEach((mm) -> {
                try {
                    out.write("\tMETHOD ");
                    out.write(mm.getObfuscatedName());
                    out.write(" ");
                    if (mm.isNameDeobfuscated()) {
                        out.write(mm.getDeobfuscatedName());
                        out.write(" ");
                    }
                    out.write(mm.getDeobfuscatedDescriptor());
                    out.write(NEXT_LINE);
                    for (int i = 0; i < mm.getParameterNames().length; i++) {
                        if (mm.getParameterNames()[i] != null) {
                            out.write("\t\tARG ");
                            out.write(i + "");
                            out.write(" ");
                            out.write(mm.getParameterNames()[i]);
                            out.write(NEXT_LINE);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }));

        try {
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
