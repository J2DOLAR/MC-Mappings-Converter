package me.j2dolar.mcmc.containers;

public class FieldMapping extends Mapping {

    private String obfuscatedType, deobfuscatedType;

    public FieldMapping(String obfuscatedName, String deobfuscatedName, String obfuscatedType, String deobfuscatedType) {
        super(obfuscatedName, deobfuscatedName);
        this.obfuscatedType = obfuscatedType;
        this.deobfuscatedType = deobfuscatedType;
    }

    public String getObfuscatedType() {
        return this.obfuscatedType;
    }

    public void setObfuscatedType(String obfuscatedType) {
        this.obfuscatedType = obfuscatedType;
    }

    public String getDeobfuscatedType() {
        return this.deobfuscatedType;
    }

    public void setDeobfuscatedType(String deobfuscatedType) {
        this.deobfuscatedType = deobfuscatedType;
    }

}
