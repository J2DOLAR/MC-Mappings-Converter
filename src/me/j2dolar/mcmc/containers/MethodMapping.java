package me.j2dolar.mcmc.containers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MethodMapping extends Mapping {

    //Source for the below patterns and getMethodParamCount method: https://github.com/alexradzin/SafeChain/blob/master/src/main/java/com/safechain/impl/InvocationChainRetriever.java
    private static Pattern allParamsPattern = Pattern.compile("(\\(.*?\\))");
    private static Pattern paramsPattern = Pattern.compile("(\\[?)(C|Z|S|I|J|F|D|B|(:?L[^;]+;))"); // Added 'B'

    private String obfuscatedDescriptor, deobfuscatedDescriptor;
    private String[] parameterNames;

    public MethodMapping(String obfuscatedName, String deobfuscatedName, String obfuscatedDescriptor, String deobfuscatedDescriptor) {
        super(obfuscatedName, deobfuscatedName);
        this.obfuscatedDescriptor = obfuscatedDescriptor;
        this.deobfuscatedDescriptor = deobfuscatedDescriptor;
        this.parameterNames = new String[getMethodParamCount(deobfuscatedDescriptor)];
    }

    public String getObfuscatedDescriptor() {
        return this.obfuscatedDescriptor;
    }

    public void setObfuscatedDescriptor(String obfuscatedDescriptor) {
        this.obfuscatedDescriptor = obfuscatedDescriptor;
    }

    public String getDeobfuscatedDescriptor() {
        return this.deobfuscatedDescriptor;
    }

    public void setDeobfuscatedDescriptor(String deobfuscatedDescriptor) {
        this.deobfuscatedDescriptor = deobfuscatedDescriptor;
    }

    public String[] getParameterNames() {
        return parameterNames;
    }

    public void setParameterNames(String[] parameterNames) {
        this.parameterNames = parameterNames;
    }

    public void nameParameter(int index, String name) {
        this.parameterNames[index] = name;
    }

    public boolean isConstructor() {
        return super.getDeobfuscatedName().equals("<init>");
    }

    private int getMethodParamCount(String methodRefType) {
        Matcher m = allParamsPattern.matcher(methodRefType);
        if (!m.find()) {
            throw new IllegalArgumentException("Method signature does not contain parameters");
        }
        String paramsDescriptor = m.group(1);
        Matcher mParam = paramsPattern.matcher(paramsDescriptor);

        int count = 0;
        while (mParam.find()) {
            count++;
        }
        return count;
    }

}
