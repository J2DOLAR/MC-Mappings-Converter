package me.j2dolar.mcmc.containers;

class Mapping {

    private String obfuscatedName, deobfuscatedName;

    public Mapping(String obfuscatedName, String deobfuscatedName) {
        this.obfuscatedName = obfuscatedName;
        this.deobfuscatedName = deobfuscatedName;
    }

    public String getObfuscatedName() {
        return this.obfuscatedName;
    }

    public void setObfuscatedName(String obfuscatedName) {
        this.obfuscatedName = obfuscatedName;
    }

    public boolean isNameDeobfuscated() {
        return this.deobfuscatedName != null;
    }

    public String getDeobfuscatedName() {
        return this.deobfuscatedName;
    }

    public void setDeobfuscatedName(String deobfuscatedName) {
        this.deobfuscatedName = deobfuscatedName;
    }
}
