package me.j2dolar.mcmc.containers;

import java.util.ArrayList;
import java.util.List;

public class ClassMapping extends Mapping {

    private final List<FieldMapping> fieldMappings;
    private final List<MethodMapping> methodMappings;

    public ClassMapping(String obfuscatedName, String deobfuscatedName) { //obfuscatedName and deobfuscatedName should include packaging
        super(obfuscatedName, deobfuscatedName);
        this.fieldMappings = new ArrayList<>();
        this.methodMappings = new ArrayList<>();
    }

    public void registerFieldMapping(FieldMapping fieldMapping) {
        this.fieldMappings.add(fieldMapping);
    }

    public void registerMethodMapping(MethodMapping methodMapping) {
        this.methodMappings.add(methodMapping);
    }

    public List<FieldMapping> getFieldMappings() {
        return this.fieldMappings;
    }

    public List<MethodMapping> getMethodMappings() {
        return this.methodMappings;
    }

}