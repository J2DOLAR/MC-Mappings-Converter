package me.j2dolar.mcmc;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.converters.FileConverter;
import me.j2dolar.mcmc.containers.ClassMapping;
import me.j2dolar.mcmc.mappings.EnigmaMappingsParser;
import me.j2dolar.mcmc.mappings.MappingsParser;
import me.j2dolar.mcmc.mappings.SrgMappingsParser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {
        Options options = new Options();
        new JCommander(options, args);

        MappingsParser inParser = getMappingsParser(options.inType);
        MappingsParser outParser = getMappingsParser(options.outType);

        System.out.println("Reading...");
        List<ClassMapping> in = inParser.read(options.inFile);
        System.out.println("Finished reading!");
        System.out.println("Writing...");
        outParser.write(options.outFile, in);
        System.out.println("Finished writing!");

        /*
        // BEGIN: enigma -> enigma testing.  The output should be identical to the input because the
        // program has enigma support at the moment.
        BufferedReader inReader = new BufferedReader(new FileReader(options.inFile));
        BufferedReader outReader = new BufferedReader(new FileReader(options.outFile));

        String nextIn;
        int fails = 0, line = 0;
        System.out.format("%72s %72s\n", "in", "out");
        while ((nextIn = inReader.readLine()) != null) {
            line++;
            String nextOut = outReader.readLine();

            if (!nextIn.equals(nextOut)) {
                fails++;
                System.out.format("Line %s: Failed on \"%72s\" \"%72s\"", line, nextIn, nextOut);
                System.out.println();
            }
        }
        inReader.close();
        outReader.close();
        System.out.println(fails + " lines weren't identical.");
        // STOP: testing
        */
    }

    private static MappingsParser getMappingsParser(String type) {
        switch (type) {
            case "enigma":
                return new EnigmaMappingsParser();
            case "srg":
                return new SrgMappingsParser();
        }
        return new EnigmaMappingsParser();
    }

    private static class Options {
        @Parameter
        public List<String> parameters = new ArrayList<>();

        @Parameter(names = {"-intype"}, description = "Type of input mapping (enigma/srg).", required = true, validateWith = MappingsTypeValidator.class)
        public String inType;

        @Parameter(names = {"-infile"}, description = "Input file.", required = true, converter = FileConverter.class)
        public File inFile;

        @Parameter(names = {"-outtype"}, description = "Type of output mapping (enigma/srg).", required = true, validateWith = MappingsTypeValidator.class)
        public String outType;

        @Parameter(names = {"-outfile"}, description = "Output file.", required = true, converter = FileConverter.class)
        public File outFile;

    }

    public static class MappingsTypeValidator implements IParameterValidator {
        @Override
        public void validate(String name, String value) throws ParameterException {
            if (!value.equalsIgnoreCase("enigma") && !value.equalsIgnoreCase("srg")) {
                throw new ParameterException("Invalid type.");
            }
        }
    }

}
